/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantida.oxprogramoop;

import java.util.Scanner;

/**
 *
 * @author Admin
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error : Row and Column is not empty.");
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }
    public void newGame(){
        table = new Table(playerX, playerO);
        System.out.println();
        System.out.println("Do You want to start a new game?(Y/N): ");
        char inp = kb.next().charAt(0);
        if(inp == 'Y'){
            run();
        }else{
            System.out.println("Thank you for enjoying the game.");
        }
      
    }
    public void showBye(){
        System.out.println("Bye Bye....");
    }
    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (table.isFinish()) {
                if (table.getWinner() == null) {
                    System.out.println("Draw!!!");
                } else {
                    System.out.println("Player "+ table.getWinner().getName() + " Win....");
                }
                
                System.out.println("");
                this.showTable();
                newGame();
                showBye();
                break;
            }
            table.switchPlayer();
            
        }
    }
}
