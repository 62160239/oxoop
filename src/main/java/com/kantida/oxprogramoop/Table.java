/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantida.oxprogramoop;

/**
 *
 * @author Admin
 */
public class Table {

    private char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;
    
    private int lastCol;
    private int lastRow;

    public Table(Player X, Player O) {
        playerX = X;
        playerO = O;
        currentPlayer = X;
    }

    public void showTable() {
        System.out.println(" 1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            return true;
        }
        return false;
    }
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    public void switchPlayer(){
        if(currentPlayer == playerX){
            currentPlayer = playerO;
        }else{
            currentPlayer = playerX;
        }
    }
    void checkColumn() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        
        setStatWinLose();
    }

    private void setStatWinLose() {
        if(currentPlayer == playerO){
            playerO.win();
            playerX.lose();
        }else{
            playerO.lose();
            playerX.win();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        
        setStatWinLose();
    }

    void checkDiagonal() {
        if (table[0][0] == table[1][1]
                && table[1][1] == table[2][2]
                && table[0][0] != '-') {
            finish = true;
            winner = currentPlayer;
            return;
        }
        if (table[0][2] == table[1][1]
                && table[1][1] == table[2][0]
                && table[0][2] != '-') {
            finish = true;
            winner = currentPlayer;
            return;
        }
    }

    boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        finish = true;
        playerO.draw();
        playerX.draw();
        return true;
        
    }

    public void checkWin() {
        checkRow();
        checkColumn();
        checkDiagonal();
        checkDraw();
    }
    
    public boolean isFinish(){
        return finish;
    }
    public Player getWinner(){
        return winner;
    }
}
